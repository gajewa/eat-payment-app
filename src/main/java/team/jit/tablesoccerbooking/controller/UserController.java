package team.jit.tablesoccerbooking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import team.jit.tablesoccerbooking.entity.AvailableAuth;
import team.jit.tablesoccerbooking.dto.UsernameAndPassword;
import team.jit.tablesoccerbooking.entity.User;
import team.jit.tablesoccerbooking.enums.AuthenticationSource;
import team.jit.tablesoccerbooking.repository.AvailableAuthRepository;
import team.jit.tablesoccerbooking.repository.UserRepository;
import team.jit.tablesoccerbooking.service.UserService;
//import team.jit.tablesoccerbooking.service.UserService;

@Api(value = "/users", description = "User controller prepared to login external users to the system.")
@RestController
@RequestMapping("/api/users")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;
    private final AvailableAuthRepository availableAuthRepository;
    private final OAuth2AuthorizedClientService authorizedClientService;

    @ApiOperation("Login/authenticate user with provided username and password to the system.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "When an user has been successfully logged/authenticated to the system."),
            @ApiResponse(code = 401, message = "When unable to authenticate (wrong username or password).")
    })

    @GetMapping("/availableAuths")
    public ResponseEntity getAvailableAuths(Model model){
        List<AvailableAuth> availableAuths = availableAuthRepository.findAllByActiveTrue();

        return ResponseEntity.status(401).body(availableAuths);
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UsernameAndPassword usernameAndPassword) {

        String token = userService.login(usernameAndPassword);

        return ResponseEntity.ok().header(HttpHeaders.AUTHORIZATION, token).build();
    }

    @GetMapping
    public ResponseEntity test(Authentication authentication) {
        return ResponseEntity.ok(authentication.getName());
    }


    @GetMapping("/loginSuccess")
    public User getLoginInfo(Model model, OAuth2AuthenticationToken authentication) {

        String externalId = isGoogleAuthenticated(authentication)
                ? authentication.getPrincipal().getAttributes().get("sub").toString()
                : authentication.getPrincipal().getAttributes().get("id").toString();

        User user = userRepository.findByExternalId(externalId)
                .orElseGet(() -> {
                    Map attributes = authentication.getPrincipal().getAttributes();

                    return userRepository.save(
                            User.builder()
                                    .name(attributes.get("name").toString())
                                    .authenticationSource(isGoogleAuthenticated(authentication) ? AuthenticationSource.GOOGLE : AuthenticationSource.FACEBOOK)
                                    .externalId(externalId)
                                    .build()
                    );
                });

        return user;
    }

    private boolean isGoogleAuthenticated(OAuth2AuthenticationToken authentication) {
        return authentication.getAuthorizedClientRegistrationId().equals("google");
    }

}
