package team.jit.tablesoccerbooking.component.user;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import team.jit.tablesoccerbooking.entity.User;

@Component
@RequiredArgsConstructor
public class TokenIssuer {

    @Value("${security.expirationTimeInMinutes}")
    Integer expirationTimeInMinutes;

    @Value("${security.issuer}")
    String issuer;

    private final KeyProvider keyProvider;

    public String issueToken(User user) {
        Key key = keyProvider.getKey();
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime expirationDateTime = currentDateTime.plusMinutes(user.getExpirationTimeInMinutes());

        return Jwts.builder()
            .setSubject(user.getUsername())
            .setIssuer(issuer)
            .setIssuedAt(Date.from(currentDateTime.atZone(ZoneId.systemDefault()).toInstant()))
            .setExpiration(Date.from(expirationDateTime.atZone(ZoneId.systemDefault()).toInstant()))
            .signWith(SignatureAlgorithm.HS512, key)
             .compact();
    }
}
