package team.jit.tablesoccerbooking.utils;

public class ExceptionMessages {

    public final static String WRONG_CREDENTIALS = "Wrong credentials.";
    public final static String ENTITY_NOT_FOUND = "Entity of given id not found.";


}
