package team.jit.tablesoccerbooking.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity<T_ID> implements Serializable {

    @NotNull
    protected LocalDateTime created;

    public abstract T_ID getId();

    @PrePersist
    public void fillCreated() {
        if (created == null) {
            created = LocalDateTime.now();
        }
    }

    @JsonIgnore
    public boolean isNew() {
        return getId() == null;
    }
}
