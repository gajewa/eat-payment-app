package team.jit.tablesoccerbooking.service;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import team.jit.tablesoccerbooking.component.user.Authenticator;
import team.jit.tablesoccerbooking.component.user.TokenIssuer;
import team.jit.tablesoccerbooking.dto.UsernameAndPassword;
import team.jit.tablesoccerbooking.entity.User;

@Service
@RequiredArgsConstructor
public class UserService {

    private final Authenticator authenticator;
    private final TokenIssuer tokenIssuer;

    public String login(UsernameAndPassword usernameAndPassword) {

        User authenticatedUser = authenticator.authenticate(usernameAndPassword);

        return tokenIssuer.issueToken(authenticatedUser);
    }
}
