For this project you will need: Java 12, Docker, Intellij and the lombok plugin for Intellij.

To setup the project pull the repository and create a new project in IntelliJ. 

Select file -> new -> project from existing sources. 

Navigate to the project directory and pick the pom.xml file.

Click OK, then Next twice, and on the select SDK pick Java 12 if it is not picked. Click next and finally click finish.

Before starting the application (shift + f10) you have to start the database with this docker command:
```
docker run --restart=always --name soccerDb -e POSTGRES_USER=dev -e POSTGRES_PASSWORD=dev -e POSTGRES_DB=soccerDb -p 5432:5432 postgres:9.5
```

AUTHENTICATION & SECURITY

Test username and password: admin

You can login with either google/facebook oauth or with the internal authentication mechanism. 

~~For logging in with google or facebook you can go to localhost:8081/login and you will see two buttons for logging in with google or facebook.
 If you are not logged in in any way and try to access the API you will get a 302 redirect to this address with the Set-Header header that contains a JSESSIONID that from now on has to be included in the Cookie header to be authenticated.~~
For logging in with google or facebook you can go to http://localhost:8081/api/users/availableAuths which will return available ways of authenticating. For example:
```
[
    {
        created: null,
        id: 1,
        url: "/api/users/login",
        active: true,
        authenticationSource: "INTERNAL"
    },  
    {   
        created: null,
        id: 2,
        url: "/oauth2/authorization/facebook",
        active: true,
        authenticationSource: "FACEBOOK"
    },
    {
        created: null,
        id: 3,
        url: "/oauth2/authorization/google",
        active: true,
        authenticationSource: "GOOGLE"
    }
]
```
Here we can see that there are three active ways of authenticating. We can turn any of them at anytime by changing the `active` flag in the available_auth table in the DB. 
For external OAuth2 authentication send a GET request to `localhost:8081 + {url}`, this will redirect you to the external login page.

To login without google/facebook oauth send a POST request to the endpoint /api/login with the follwoing body:
```
{
	"username": "admin",
	"password": "admin"
}
```

If the login is successful the response will be 200 OK and a header with the token used for authorization:
```
Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlzcyI6ImppdCIsImlhdCI6MTU2MTA2MjUzOCwiZXhwIjoxNTYxMDY2MTM4fQ.G_stEMG3ShcpXQWfp5F-_ci2ZD1koLcFy3Ggwyyb6hOHNgf7hDte2t_rBleurLA3h9JiPEmHBUxp4TF581uogg
```

If not there will be the following body:
```
{
    "message": "Wrong credentials.",
    "description": "Unable to authenticate user = admin with given password. Login or password is incorrect.",
    "stackTrace": null,
    "dateTime": "20-06-2019 22:29:53",
    "violations": null
}
```

From that point on to authenticate the user all requests have to have the Authorization header with the given token. For now you can check if you can successfuly authenticate at this endpoit: `GET /api/user`. If all is correct it will return the username of the user.

If not this will be the response:
```
{
    "timestamp": "2019-06-20T20:32:14.186+0000",
    "status": 401,
    "error": "Unauthorized",
    "message": "Access Denied, your token has been expired or you should provide valid JWT token.",
    "path": "/api/user/d"
}
```