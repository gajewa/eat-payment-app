package team.jit.tablesoccerbooking.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import team.jit.tablesoccerbooking.entity.User;


public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsernameAndPassword(String username, String hashedPassword);

    Optional<User> findByExternalId(String externalId);
}
