package team.jit.tablesoccerbooking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import team.jit.tablesoccerbooking.entity.Bill;
import team.jit.tablesoccerbooking.entity.BillEntry;

@Repository
public interface BillEntryRepository extends JpaRepository<BillEntry, Long> {

    Optional<BillEntry> findById(Long id);

}
