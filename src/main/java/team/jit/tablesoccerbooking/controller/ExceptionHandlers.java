package team.jit.tablesoccerbooking.controller;

import java.time.LocalDateTime;

import javax.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import team.jit.tablesoccerbooking.dto.ErrorMessage;
import team.jit.tablesoccerbooking.exception.AuthenticationException;
import team.jit.tablesoccerbooking.utils.ExceptionMessages;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlers {

    @ExceptionHandler
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException e) {
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(ExceptionMessages.WRONG_CREDENTIALS)
                .dateTime(LocalDateTime.now())
                .description(e.getMessage())
                .build();

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(errorMessage);
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException e) {
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(ExceptionMessages.ENTITY_NOT_FOUND)
                .dateTime(LocalDateTime.now())
                .description(e.getMessage())
                .build();

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(errorMessage);
    }
}



