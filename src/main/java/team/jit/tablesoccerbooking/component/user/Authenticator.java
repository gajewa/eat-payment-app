package team.jit.tablesoccerbooking.component.user;

import java.text.MessageFormat;
import java.util.Optional;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import team.jit.tablesoccerbooking.dto.UsernameAndPassword;
import team.jit.tablesoccerbooking.entity.User;
import team.jit.tablesoccerbooking.exception.AuthenticationException;
import team.jit.tablesoccerbooking.repository.UserRepository;

@Slf4j
@Component
@RequiredArgsConstructor
public class Authenticator {

    private static final String AUTHENTICATION_ERROR_MESSAGE = "Unable to authenticate user = {0} with given password. Login or password is incorrect.";

    private final UserRepository userRepository;

    public User authenticate(UsernameAndPassword usernameAndPassword) {
        log.info("Authenticating user = {}.", usernameAndPassword.getUsername());

        String hashedPassword = DigestUtils.sha512Hex(usernameAndPassword.getPassword());

        Optional<User> user = userRepository.findByUsernameAndPassword(usernameAndPassword.getUsername(), hashedPassword);

        if (user.isPresent()) {
            log.info("Authentication successful for user = {}.", usernameAndPassword.getUsername());
            return user.get();
        } else {
            log.warn("Unable to authenticate user = {}. Probable wrong login or password!", usernameAndPassword.getUsername());
            throw new AuthenticationException(MessageFormat.format(AUTHENTICATION_ERROR_MESSAGE, usernameAndPassword.getUsername()));
        }
    }
}
