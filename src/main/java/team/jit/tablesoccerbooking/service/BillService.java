package team.jit.tablesoccerbooking.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;
import team.jit.tablesoccerbooking.dto.AccessTokenResponse;
import team.jit.tablesoccerbooking.entity.Bill;
import team.jit.tablesoccerbooking.entity.BillEntry;
import team.jit.tablesoccerbooking.entity.User;
import team.jit.tablesoccerbooking.repository.BillEntryRepository;
import team.jit.tablesoccerbooking.repository.BillRepository;
import team.jit.tablesoccerbooking.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class BillService {

    private final BillRepository billRepository;
    private final UserRepository userRepository;
    private final BillEntryRepository billEntryRepository;

    public void save(Bill bill){
        billRepository.save(bill);
    }

    public List<Bill> findAll() {
        return billRepository.findAll();
    }

    public Bill findById(Long id){
        return billRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    public List<Bill> findByOwnerId(Long userId){
        return billRepository.findAllByOwnerId(userId);
    }

    public void addBillEntry(Long billId, Long userId, BillEntry billEntry) {
        Bill bill = billRepository.findById(billId).orElseThrow(EntityNotFoundException::new);
        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::new);

        billEntry = billEntry.toBuilder().bill(bill).user(user).build();

        billEntryRepository.save(billEntry);
    }

    public void save(Long ownerId, Bill bill) {
        User user = userRepository.findById(ownerId).orElseThrow(EntityNotFoundException::new);

        bill = bill.toBuilder().owner(user).build();

        billRepository.save(bill);
    }

    public void payEntry(Long billId) {
        RestTemplate restTemplate = new RestTemplateBuilder().build();

        ResponseEntity<AccessTokenResponse> accessTokenResponse = restTemplate.postForEntity("https://secure.snd.payu.com/pl/standard/user/oauth/authorize?grant_type=client_credentials&client_id=367723&client_secret=ff1a59bd47e88cf74d1453d01d3c8a63", null, AccessTokenResponse.class);

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(accessTokenResponse.getBody().getAccess_token());

        int i = 0;
    }
}
