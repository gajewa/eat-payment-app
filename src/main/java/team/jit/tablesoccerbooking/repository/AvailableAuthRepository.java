package team.jit.tablesoccerbooking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import team.jit.tablesoccerbooking.entity.AvailableAuth;

@Repository
public interface AvailableAuthRepository extends JpaRepository<AvailableAuth, Long> {

    public List<AvailableAuth> findAllByActiveTrue();

}
