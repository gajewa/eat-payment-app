package team.jit.tablesoccerbooking.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import team.jit.tablesoccerbooking.enums.AuthenticationSource;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AvailableAuth extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String url;

    private Boolean active;

    @Enumerated(EnumType.STRING)
    private AuthenticationSource authenticationSource;

}
