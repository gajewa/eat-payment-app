package team.jit.tablesoccerbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableSoccerBookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TableSoccerBookingApplication.class, args);
	}

}
