package team.jit.tablesoccerbooking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessTokenResponse {

    private String access_token;

    private String token_type;

    private String expires_in;

    private String grant_type;
}
