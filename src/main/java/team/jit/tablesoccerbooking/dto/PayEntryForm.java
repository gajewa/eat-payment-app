package team.jit.tablesoccerbooking.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayEntryForm {

    private String notifyUrl;

    private String customerIp;

    private String merchantPosId;

    private String description;

    private String currencyCode;

    private String totalAmount;

    private PayEntryBuyerForm buyer;



    private List<ProductForm> products;

}
