package team.jit.tablesoccerbooking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductForm {

    private String name;

    private String unitPrice;

    private String quantity;

}
