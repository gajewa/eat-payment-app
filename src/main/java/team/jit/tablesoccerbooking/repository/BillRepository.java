package team.jit.tablesoccerbooking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import team.jit.tablesoccerbooking.entity.Bill;
import team.jit.tablesoccerbooking.entity.Reservation;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {

    Optional<Bill> findById(Long id);

    List<Bill> findAllByOwnerId(Long userId);

}
