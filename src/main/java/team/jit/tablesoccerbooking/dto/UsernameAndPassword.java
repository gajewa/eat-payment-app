package team.jit.tablesoccerbooking.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsernameAndPassword {

    private String username;

    private String password;
}
