package team.jit.tablesoccerbooking.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import team.jit.tablesoccerbooking.entity.Reservation;
import team.jit.tablesoccerbooking.repository.ReservationRepository;

@Service
@RequiredArgsConstructor
public class ReservationService {

    private final ReservationRepository reservationRepository;

    public void save(Reservation reservation){
        reservationRepository.save(reservation);
    }

    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    public Reservation findById(Long id){
        return reservationRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    public List<Reservation> findByUserId(Long userId){
        return reservationRepository.findAllByUserId(userId);
    }
}
