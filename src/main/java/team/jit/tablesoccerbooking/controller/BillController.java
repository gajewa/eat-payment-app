package team.jit.tablesoccerbooking.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import team.jit.tablesoccerbooking.dto.AddBillForm;
import team.jit.tablesoccerbooking.entity.BillEntry;
import team.jit.tablesoccerbooking.service.BillService;

@RestController
@RequestMapping("/api/bills")
@RequiredArgsConstructor
public class BillController {

    private final BillService billService;

    @PostMapping("/save")
    public void save(@RequestBody AddBillForm billForm) {
        billService.save(billForm.getOwnerId(), billForm.getBill());
    }

    @GetMapping("/findAll")
    public ResponseEntity findAll() {
        return ResponseEntity.ok(billService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        return ResponseEntity.ok(billService.findById(id));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity findByUserId(@PathVariable Long userId) {
        return ResponseEntity.ok(billService.findByOwnerId(userId));
    }

    @PostMapping("/{billId}/addEntry/{userId}")
    public void addEntry(@PathVariable Long billId, @PathVariable Long userId, @RequestBody BillEntry billEntry) {
        billService.addBillEntry(billId, userId, billEntry);
    }

    @PostMapping("/{billId}/payAllEntries/{userId}")
    public void payAllEntries(@PathVariable Long billId, @PathVariable Long userId, @RequestBody BillEntry billEntry) {
        billService.addBillEntry(billId, userId, billEntry);
    }

    @PostMapping("/payEntry/{billEntryId}/")
    public void addEntry(@PathVariable Long billEntryId) {
        billService.payEntry(billEntryId);
    }

}
