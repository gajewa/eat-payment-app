package team.jit.tablesoccerbooking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayEntryBuyerForm {

    private String email;

    private String firstName;

    private String lastName;

    private String phone;

    private String language;

}
