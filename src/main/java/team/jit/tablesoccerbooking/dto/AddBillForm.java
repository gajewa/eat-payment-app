package team.jit.tablesoccerbooking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import team.jit.tablesoccerbooking.entity.Bill;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddBillForm {

    private Long ownerId;

    private Bill bill;
}
