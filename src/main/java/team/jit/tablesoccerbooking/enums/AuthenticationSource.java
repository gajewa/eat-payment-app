package team.jit.tablesoccerbooking.enums;

public enum AuthenticationSource {
    FACEBOOK, GOOGLE, INTERNAL
}
